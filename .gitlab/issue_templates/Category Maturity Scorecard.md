<!--
 
Title should be: Category Maturity Scorecard - {{Stage Group}} FY{{YY}}-Q{{quarter number}} - {{Title or Description of the Evaluated Workflow / JTBD}}
(e.g. “Category Maturity Scorecard - Create:Source Code FY21-Q1 - Obtaining screenshots from testing artifacts)

This template is meant to document results from Category Maturity Scorecard user interview sessions.

If this CM Scorecard is related to an OKR, append ~OKR to the /label quick action below to automatically add the 'OKR' label.

-->

/label ~"CM scorecard" 

- **Research issue**: {{add link to research issue for the CM Scorecard research process}}
- **Previous score and scorecard**: {{if applicable, add CM Scorecard score and link to scorecard issue}}
- **Walkthrough**: {{add link to YouTube video or walkthrough document}}
- **Recommendations**: {{add link to your recommendation issue/s}}

## Category Maturity Scorecard Checklist

[Learn more about Category Maturity Scorecards](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/)
1. [ ] Review the Category Maturity Scorecard handbook page and follow the process as described. Reach out to the [UX Researcher for your stage](https://about.gitlab.com/handbook/product/categories/) if you have questions.
1. [ ] Document the results of each participant's session using the CM Scorecard Results Template
1. [ ] Add Zoom links for each participants' session recordings. These can be found in the *Recordings* section of the Zoom web UI.
1. [ ] If the participant has not granted permission to share the recording publicly, ensure the sharing settings are set to GitLab-only.
1. [ ] Summarize the results and give the final grade using this template.
1. [ ] [Create a recommendation issue](https://gitlab.com/gitlab-org/gitlab-design/issues/new?issuable_template=UX%20Scorecard%20Part%202) for these sessions.

# CM Scorecard Results Template
### Jobs to be done
{{add JTBD here}}

### Scenarios
#### Scenario N
{{add scenario prompt here}}

### Participant N
#### Background
{{Summarize the participant's role, day-to-day responsibilities, and previous GitLab usage}}

{{add link to session video}}

#### Scenario N
* Ease of completion (pre-task): 
* Ease of completion (post-task): 
* Task completion confidence: 
* Meaningful clicks: 
* Heuristic evaluation (How well did the participant complete the scenario?): 

#### Other notes
{{Add any additional notes that came from freeform discussion or elsewhere}}

### Overall Grade: X

This grade represents the average of the ratings for all participants based on the [grading rubric](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/#grading)
