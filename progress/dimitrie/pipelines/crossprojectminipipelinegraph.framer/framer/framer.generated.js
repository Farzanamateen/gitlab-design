// This is autogenerated by Framer


if (!window.Framer && window._bridge) {window._bridge('runtime.error', {message:'[framer.js] Framer library missing or corrupt. Select File → Update Framer Library.'})}
window.__imported__ = window.__imported__ || {};
window.__imported__["pipelines-overview-stages@1x/layers.json.js"] = [
	{
		"objectId": "6A0FF2F5-9374-46A9-9E8A-B3738ABF8C06",
		"kind": "artboard",
		"name": "Mini_pipeline_graph_active_Copy",
		"originalName": "Mini-pipeline-graph-active Copy",
		"maskFrame": null,
		"layerFrame": {
			"x": 363,
			"y": 535,
			"width": 311,
			"height": 151
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"image": {
			"path": "images/Layer-Mini_pipeline_graph_active_Copy-nkewrkyy.png",
			"frame": {
				"x": 363,
				"y": 535,
				"width": 311,
				"height": 151
			}
		},
		"children": [
			{
				"objectId": "7EE7E1F3-A725-4A87-A467-FAB9F1D28132",
				"kind": "group",
				"name": "tooltip",
				"originalName": "tooltip",
				"maskFrame": null,
				"layerFrame": {
					"x": 151,
					"y": 55,
					"width": 115,
					"height": 29
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-tooltip-n0vfn0ux.png",
					"frame": {
						"x": 151,
						"y": 55,
						"width": 115,
						"height": 29
					}
				},
				"children": []
			},
			{
				"objectId": "9A7DEA05-EEC0-4711-AC4F-62663F806171",
				"kind": "group",
				"name": "mini_pipeline_graph",
				"originalName": "mini-pipeline-graph",
				"maskFrame": null,
				"layerFrame": {
					"x": 94,
					"y": 85,
					"width": 81,
					"height": 22
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "EE524338-0245-4C4A-A222-FC9451E7F2D3",
						"kind": "group",
						"name": "icon_job_passed_22px_copy_7",
						"originalName": "icon-job-passed-22px copy 7",
						"maskFrame": null,
						"layerFrame": {
							"x": 152,
							"y": 85,
							"width": 23,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "1C7B6920-1FBE-4095-867F-32D4463C5679",
								"kind": "group",
								"name": "icon_job_passed_22px",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 152.545454545454,
									"y": 85,
									"width": 23,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-mum3qjy5.png",
									"frame": {
										"x": 152.545454545454,
										"y": 85,
										"width": 23,
										"height": 22
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "E5B94F72-FDCD-413D-8D7D-C0084FC789AA",
						"kind": "group",
						"name": "icon_job_passed_22px_copy_6",
						"originalName": "icon-job-passed-22px copy 6",
						"maskFrame": null,
						"layerFrame": {
							"x": 123,
							"y": 85,
							"width": 23,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "739BF815-6C33-4107-831F-7F28C3878084",
								"kind": "group",
								"name": "icon_job_passed_22px1",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 123.54545454545405,
									"y": 85,
									"width": 23,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-nzm5qky4.png",
									"frame": {
										"x": 123.54545454545405,
										"y": 85,
										"width": 23,
										"height": 22
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "F5A60A1E-B028-47FE-A438-D36E916C8B4A",
						"kind": "group",
						"name": "icon_job_passed_22px_copy_4",
						"originalName": "icon-job-passed-22px copy 4",
						"maskFrame": null,
						"layerFrame": {
							"x": 94,
							"y": 85,
							"width": 23,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "52B69923-6CC6-48FB-A63E-56E98ED197FA",
								"kind": "group",
								"name": "icon_job_passed_22px2",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 94.54545454545405,
									"y": 85,
									"width": 23,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-ntjcnjk5.png",
									"frame": {
										"x": 94.54545454545405,
										"y": 85,
										"width": 23,
										"height": 22
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "F6A5F5E0-F86C-412D-9901-07CDE71B4F38",
				"kind": "group",
				"name": "downstream_group",
				"originalName": "downstream-group",
				"maskFrame": null,
				"layerFrame": {
					"x": 199,
					"y": 86,
					"width": 43,
					"height": 20
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "B14AC1F4-F754-4DF2-BACF-C293483340FA",
						"kind": "group",
						"name": "downstream_1",
						"originalName": "downstream-1",
						"maskFrame": null,
						"layerFrame": {
							"x": 199,
							"y": 86,
							"width": 21,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "3098E470-A142-4BDD-AA5E-43D9B426A90B",
								"kind": "group",
								"name": "icon_job_passed_22px3",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 199,
									"y": 86,
									"width": 21,
									"height": 20
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-mza5oeu0.png",
									"frame": {
										"x": 199,
										"y": 86,
										"width": 21,
										"height": 20
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "7CF1A57D-77B5-47A9-9F07-867A739C4997",
						"kind": "group",
						"name": "downstream_2",
						"originalName": "downstream-2",
						"maskFrame": null,
						"layerFrame": {
							"x": 211,
							"y": 86,
							"width": 21,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "443F4810-20D4-489D-BEF4-549DDAB7A132",
								"kind": "group",
								"name": "icon_job_passed_22px4",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 211,
									"y": 86,
									"width": 21,
									"height": 20
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-ndqzrjq4.png",
									"frame": {
										"x": 211,
										"y": 86,
										"width": 21,
										"height": 20
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "ADA1047A-A194-4C23-AE1E-E677C6181D58",
						"kind": "group",
						"name": "downstream_3",
						"originalName": "downstream-3",
						"maskFrame": null,
						"layerFrame": {
							"x": 223,
							"y": 87,
							"width": 19,
							"height": 18
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "1C3F0047-D58F-4D8F-9EFB-D94CB62C8620",
								"kind": "group",
								"name": "icon_job_passed_22px5",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 223,
									"y": 87,
									"width": 19,
									"height": 18
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-mumzrjaw.png",
									"frame": {
										"x": 223,
										"y": 87,
										"width": 19,
										"height": 18
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "14258EB6-DCE3-44A8-8F40-5478628AF075",
				"kind": "group",
				"name": "upstream",
				"originalName": "upstream",
				"maskFrame": null,
				"layerFrame": {
					"x": 51,
					"y": 87,
					"width": 19,
					"height": 18
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "77D311E3-1278-42DD-A03F-1BA9B4E643D8",
						"kind": "group",
						"name": "icon_job_passed_22px6",
						"originalName": "icon-job-passed-22px",
						"maskFrame": null,
						"layerFrame": {
							"x": 51,
							"y": 87,
							"width": 19,
							"height": 18
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-icon_job_passed_22px-nzdemzex.png",
							"frame": {
								"x": 51,
								"y": 87,
								"width": 19,
								"height": 18
							}
						},
						"children": []
					}
				]
			}
		]
	}
]
window.__imported__ = window.__imported__ || {};
window.__imported__["pipelines-overview-stages@2x/layers.json.js"] = [
	{
		"objectId": "6A0FF2F5-9374-46A9-9E8A-B3738ABF8C06",
		"kind": "artboard",
		"name": "Mini_pipeline_graph_active_Copy",
		"originalName": "Mini-pipeline-graph-active Copy",
		"maskFrame": null,
		"layerFrame": {
			"x": 363,
			"y": 535,
			"width": 311,
			"height": 151
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"image": {
			"path": "images/Layer-Mini_pipeline_graph_active_Copy-nkewrkyy.png",
			"frame": {
				"x": 363,
				"y": 535,
				"width": 311,
				"height": 151
			}
		},
		"children": [
			{
				"objectId": "7EE7E1F3-A725-4A87-A467-FAB9F1D28132",
				"kind": "group",
				"name": "tooltip",
				"originalName": "tooltip",
				"maskFrame": null,
				"layerFrame": {
					"x": 151,
					"y": 55,
					"width": 115,
					"height": 29
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-tooltip-n0vfn0ux.png",
					"frame": {
						"x": 151,
						"y": 55,
						"width": 115,
						"height": 29
					}
				},
				"children": []
			},
			{
				"objectId": "9A7DEA05-EEC0-4711-AC4F-62663F806171",
				"kind": "group",
				"name": "mini_pipeline_graph",
				"originalName": "mini-pipeline-graph",
				"maskFrame": null,
				"layerFrame": {
					"x": 94,
					"y": 85,
					"width": 81,
					"height": 22
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "EE524338-0245-4C4A-A222-FC9451E7F2D3",
						"kind": "group",
						"name": "icon_job_passed_22px_copy_7",
						"originalName": "icon-job-passed-22px copy 7",
						"maskFrame": null,
						"layerFrame": {
							"x": 152,
							"y": 85,
							"width": 23,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "1C7B6920-1FBE-4095-867F-32D4463C5679",
								"kind": "group",
								"name": "icon_job_passed_22px",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 152.545454545454,
									"y": 85,
									"width": 23,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-mum3qjy5.png",
									"frame": {
										"x": 152.545454545454,
										"y": 85,
										"width": 23,
										"height": 22
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "E5B94F72-FDCD-413D-8D7D-C0084FC789AA",
						"kind": "group",
						"name": "icon_job_passed_22px_copy_6",
						"originalName": "icon-job-passed-22px copy 6",
						"maskFrame": null,
						"layerFrame": {
							"x": 123,
							"y": 85,
							"width": 23,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "739BF815-6C33-4107-831F-7F28C3878084",
								"kind": "group",
								"name": "icon_job_passed_22px1",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 123.54545454545405,
									"y": 85,
									"width": 23,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-nzm5qky4.png",
									"frame": {
										"x": 123.54545454545405,
										"y": 85,
										"width": 23,
										"height": 22
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "F5A60A1E-B028-47FE-A438-D36E916C8B4A",
						"kind": "group",
						"name": "icon_job_passed_22px_copy_4",
						"originalName": "icon-job-passed-22px copy 4",
						"maskFrame": null,
						"layerFrame": {
							"x": 94,
							"y": 85,
							"width": 23,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "52B69923-6CC6-48FB-A63E-56E98ED197FA",
								"kind": "group",
								"name": "icon_job_passed_22px2",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 94.54545454545405,
									"y": 85,
									"width": 23,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-ntjcnjk5.png",
									"frame": {
										"x": 94.54545454545405,
										"y": 85,
										"width": 23,
										"height": 22
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "F6A5F5E0-F86C-412D-9901-07CDE71B4F38",
				"kind": "group",
				"name": "downstream_group",
				"originalName": "downstream-group",
				"maskFrame": null,
				"layerFrame": {
					"x": 199,
					"y": 86,
					"width": 43,
					"height": 20
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "B14AC1F4-F754-4DF2-BACF-C293483340FA",
						"kind": "group",
						"name": "downstream_1",
						"originalName": "downstream-1",
						"maskFrame": null,
						"layerFrame": {
							"x": 199,
							"y": 86,
							"width": 21,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "3098E470-A142-4BDD-AA5E-43D9B426A90B",
								"kind": "group",
								"name": "icon_job_passed_22px3",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 199,
									"y": 86,
									"width": 21,
									"height": 20
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-mza5oeu0.png",
									"frame": {
										"x": 199,
										"y": 86,
										"width": 21,
										"height": 20
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "7CF1A57D-77B5-47A9-9F07-867A739C4997",
						"kind": "group",
						"name": "downstream_2",
						"originalName": "downstream-2",
						"maskFrame": null,
						"layerFrame": {
							"x": 211,
							"y": 86,
							"width": 21,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "443F4810-20D4-489D-BEF4-549DDAB7A132",
								"kind": "group",
								"name": "icon_job_passed_22px4",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 211,
									"y": 86,
									"width": 21,
									"height": 20
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-ndqzrjq4.png",
									"frame": {
										"x": 211,
										"y": 86,
										"width": 21,
										"height": 20
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "ADA1047A-A194-4C23-AE1E-E677C6181D58",
						"kind": "group",
						"name": "downstream_3",
						"originalName": "downstream-3",
						"maskFrame": null,
						"layerFrame": {
							"x": 223,
							"y": 87,
							"width": 19,
							"height": 18
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "1C3F0047-D58F-4D8F-9EFB-D94CB62C8620",
								"kind": "group",
								"name": "icon_job_passed_22px5",
								"originalName": "icon-job-passed-22px",
								"maskFrame": null,
								"layerFrame": {
									"x": 223,
									"y": 87,
									"width": 19,
									"height": 18
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon_job_passed_22px-mumzrjaw.png",
									"frame": {
										"x": 223,
										"y": 87,
										"width": 19,
										"height": 18
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "14258EB6-DCE3-44A8-8F40-5478628AF075",
				"kind": "group",
				"name": "upstream",
				"originalName": "upstream",
				"maskFrame": null,
				"layerFrame": {
					"x": 51,
					"y": 87,
					"width": 19,
					"height": 18
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "77D311E3-1278-42DD-A03F-1BA9B4E643D8",
						"kind": "group",
						"name": "icon_job_passed_22px6",
						"originalName": "icon-job-passed-22px",
						"maskFrame": null,
						"layerFrame": {
							"x": 51,
							"y": 87,
							"width": 19,
							"height": 18
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-icon_job_passed_22px-nzdemzex.png",
							"frame": {
								"x": 51,
								"y": 87,
								"width": 19,
								"height": 18
							}
						},
						"children": []
					}
				]
			}
		]
	}
]
if (DeviceComponent) {DeviceComponent.Devices["iphone-6-silver"].deviceImageJP2 = false};
if (window.Framer) {window.Framer.Defaults.DeviceView = {"deviceScale":1,"selectedHand":"","deviceType":"fullscreen","contentScale":1,"hideBezel":true,"orientation":0};
}
if (window.Framer) {window.Framer.Defaults.DeviceComponent = {"deviceScale":1,"selectedHand":"","deviceType":"fullscreen","contentScale":1,"hideBezel":true,"orientation":0};
}
window.FramerStudioInfo = {"deviceImagesUrl":"\/_server\/resources\/DeviceImages","documentTitle":"crossprojectminipipelinegraph.framer"};

Framer.Device = new Framer.DeviceView();
Framer.Device.setupContext();